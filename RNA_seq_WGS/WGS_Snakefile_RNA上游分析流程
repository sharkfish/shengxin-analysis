from glob import iglob 
import pandas as pd

r1 = iglob('/public/home/sxgou_gibh/yhl_20220128/Seq_Data/RNA-seq/*_R1_001.fastq.gz')

samples = pd.DataFrame()
samples['r1'] = [i for i in r1]
samples['cell'] = samples.r1.str.extract('/public/home/sxgou_gibh/yhl_20220128/Seq_Data/RNA-seq/(.*)_R1_001\.fastq\.gz', expand=True)

samples2 =  samples.loc[~samples.cell.isin(["WT1", "WT2", "WT3"]), :]

rule all:
	input:
		#list('Trim/' + samples['cell'] + '_R1_val_1.fq.gz'),
		#list('Trim/' + samples['cell'] + '_R2_val_2.fq.gz'),
		#list('Mapping/' + samples['cell'] + '_Aligned.sortedByCoord.out.bam'),
		#list('Mapping/' + samples['cell'] + '.addhead.bam'),
		#list('MarkDup/' + samples['cell'] + '.markdup.bam'),
		#list('MarkDup/' + samples['cell'] + '.metrics.txt'),
		#list('MarkDup/' + samples['cell'] + '.split.bam'),
		#list('MarkDup/' + samples['cell'] + '.split.bam.bai'),
		#list('BQSR/' + samples['cell']  + '.split.recal.table'),
		#list('BQSR/' + samples['cell']  + '.BQSR.bam'),
		#list('BQSR/' + samples['cell'] + '.BQSR.bam.bai'),
		list('Raw_Variants/' + samples['cell'] + '.HC.vcf.gz'),
		list('SnpEff_filter/' + samples['cell'] + '.vcf'),
		list('Split_Variants/' + samples['cell'] + '.snp.vcf.gz'),
		list('Split_Variants/' + samples['cell'] + '.indel.vcf.gz'),
		list('PASS_Variants/' + samples['cell'] + '.snp.vcf'),
		list('PASS_Variants/' + samples['cell'] + '.indel.vcf'),
		list('HeatMap_Data/' + samples['cell'] + '_AtoC_snp.vcf'),
		list('HeatMap_Data/' + samples['cell'] + '_AtoG_snp.vcf'),
		list('HeatMap_Data/' + samples['cell'] + '_AtoT_snp.vcf'),
		list('HeatMap_Data/' + samples['cell'] + '_CtoA_snp.vcf'),
		list('HeatMap_Data/' + samples['cell'] + '_CtoG_snp.vcf'),
		list('HeatMap_Data/' + samples['cell'] + '_CtoT_snp.vcf'),
		list('HeatMap_Data/' + samples['cell'] + '_GtoA_snp.vcf'),
		list('HeatMap_Data/' + samples['cell'] + '_GtoC_snp.vcf'),
		list('HeatMap_Data/' + samples['cell'] + '_GtoT_snp.vcf'),
		list('HeatMap_Data/' + samples['cell'] + '_TtoA_snp.vcf'),
		list('HeatMap_Data/' + samples['cell'] + '_TtoC_snp.vcf'),
		list('HeatMap_Data/' + samples['cell'] + '_TtoG_snp.vcf'),
		list('CtoT_annovar/Input/' + samples['cell'] + '_CtoT.filtered.snp.vcf'),
		list('CtoT_annovar/Input/' + samples['cell'] + '_CtoA.filtered.snp.vcf'),
		list('CtoT_annovar/Input/' + samples['cell'] + '_CtoG.filtered.snp.vcf'),
		list('CtoT_annovar/Input/' + samples['cell'] + '_AtoT.filtered.snp.vcf'),
		list('CtoT_annovar/Input/' + samples['cell'] + '_AtoC.filtered.snp.vcf'),
		list('CtoT_annovar/Input/' + samples['cell'] + '_AtoG.filtered.snp.vcf'),
		list('CtoT_annovar/' + samples['cell'] + '_CtoT.snp.hg38_multianno.txt'),
		list('CtoT_annovar/' + samples['cell'] + '_CtoA.snp.hg38_multianno.txt'),
		list('CtoT_annovar/' + samples['cell'] + '_CtoG.snp.hg38_multianno.txt'),
		list('CtoT_annovar/' + samples['cell'] + '_AtoG.snp.hg38_multianno.txt'),
		list('CtoT_annovar/' + samples['cell'] + '_AtoT.snp.hg38_multianno.txt'),
		list('CtoT_annovar/' + samples['cell'] + '_AtoC.snp.hg38_multianno.txt'),
		list('Annovar_all/' + samples['cell'] + '.snp.hg38_multianno.txt'),
		list('Annovar_all/' + samples['cell'] + '.indel.hg38_multianno.txt')
		
rule Trim:
	input:
		R1 = '/public/home/sxgou_gibh/yhl_20220128/Seq_Data/RNA-seq/{cell}_R1_001.fastq.gz',
		R2 = '/public/home/sxgou_gibh/yhl_20220128/Seq_Data/RNA-seq/{cell}_R2_001.fastq.gz'
	output:
		R1 = 'Trim/{cell}_R1_val_1.fq.gz',
		R2 = 'Trim/{cell}_R2_val_2.fq.gz'
	shell:
		'''
		fastp \
		-w 20 -l 25 \
		--detect_adapter_for_pe \
		-i {input.R1} -I {input.R2} \
		-o {output.R1} -O {output.R2}
		'''
rule star:
	input:
		R1 = 'Trim/{cell}_R1_val_1.fq.gz',
		R2 = 'Trim/{cell}_R2_val_2.fq.gz'
	output:
		"Mapping/{cell}_Aligned.sortedByCoord.out.bam"
	shell:
		'''
			STAR --runThreadN 20 \
			--genomeDir /public/home/sxgou_gibh/Genomic/Homo_sapiens/UCSC/STAR_index \
			--readFilesCommand gunzip -c \
			--readFilesIn {input.R1} {input.R2} \
			--outSAMtype BAM SortedByCoordinate \
			--outFileNamePrefix Mapping/{wildcards.cell}_
		'''
rule addhead:
	input:
		"Mapping/{cell}_Aligned.sortedByCoord.out.bam"
	output:
		"Mapping/{cell}.addhead.bam"
	shell:
		'''
			gatk AddOrReplaceReadGroups \
			-I {input} \
			-O {output} \
			-LB {wildcards.cell} \
			-PL illumina \
			-PU {wildcards.cell} \
			-SM {wildcards.cell} \
			-SO coordinate
		'''
rule markdup:
	input:
		"Mapping/{cell}.addhead.bam"
	output:
		bam = "MarkDup/{cell}.markdup.bam",
		met = "MarkDup/{cell}.metrics.txt"
	shell:
		'''
			gatk MarkDuplicates \
			-I {input} \
			-O {output.bam} \
			-M {output.met}
		'''
rule SplitNC:
	input:
		bam = "MarkDup/{cell}.markdup.bam",
		ref = "/public/home/sxgou_gibh/Genomic/Homo_sapiens/UCSC/hg38.chrom.fasta"
	output:
		"MarkDup/{cell}.split.bam"
	shell:
		'''
			gatk SplitNCigarReads \
			-R {input.ref} \
			-I {input.bam} \
			-O {output}
		'''
rule index:
	input:
		"MarkDup/{cell}.split.bam"
	output:
		"MarkDup/{cell}.split.bam.bai"
	shell:
		'samtools index {input}'
rule BaseRecalibrator:
	input:
		bam = 'MarkDup/{cell}.split.bam',
		ref = '/public/home/sxgou_gibh/Genomic/Homo_sapiens/UCSC/hg38.chrom.fasta',
		db146 = '/public/home/sxgou_gibh/Genomic/Homo_sapiens/gatk/dbsnp_146.hg38.vcf.gz'
	output:
		'BQSR/{cell}.split.recal.table'
	shell:
		'''
			gatk BaseRecalibrator \
			-R {input.ref} \
			-I {input.bam} \
			--known-sites {input.db146} \
			--use-original-qualities \
			-O {output}
		'''
rule ApplyBQSR:
	input:
		bam = 'MarkDup/{cell}.split.bam',
		table = 'BQSR/{cell}.split.recal.table',
		ref = '/public/home/sxgou_gibh/Genomic/Homo_sapiens/UCSC/hg38.chrom.fasta'
	output:
		'BQSR/{cell}.BQSR.bam'
	shell:
		'''
			gatk ApplyBQSR \
			--use-original-qualities \
			--bqsr-recal-file {input.table} \
			-R {input.ref} \
			-I {input.bam} \
			-O {output}
		'''
rule bam_index:
	input:
		'BQSR/{cell}.BQSR.bam'
	output:
		'BQSR/{cell}.BQSR.bam.bai'
	shell:
		'samtools index {input}'
rule Vcall:
	input:
		bam = 'BQSR/{cell}.BQSR.bam',
		ref = '/public/home/sxgou_gibh/Genomic/Homo_sapiens/UCSC/hg38.chrom.fasta'
	output:
		'Raw_Variants/{cell}.HC.vcf.gz'
	shell:
		'''
			gatk HaplotypeCaller \
			--min-pruning 0 \
			--dont-use-soft-clipped-bases \
			--linked-de-bruijn-graph \
			--recover-all-dangling-branches \
			-R {input.ref} \
			-I {input.bam} \
			-O {output}
		'''
rule SnpEff_filter:
	input:
		'Raw_Variants/{cell}.HC.vcf.gz'
	output:
		'SnpEff_filter/{cell}.vcf'
	shell:
		'''
			zcat {input} | java -jar /public/home/sxgou_gibh/Software/shares/snpEff/SnpSift.jar filter \
			"( QUAL >= 20 ) & ( DP >= 20 ) & ( QD >= 2 ) & ( MQ >= 20 ) & ( FS <= 6 ) & ( SOR <= 2 ) & ( GEN[0].AD[1] >= 3 )" > {output}
		'''
rule SelectVariants:
	input:
		'SnpEff_filter/{cell}.vcf'
	output:
		snp = 'Split_Variants/{cell}.snp.vcf.gz',
		indel = 'Split_Variants/{cell}.indel.vcf.gz'
	shell:
		'''
			gatk SelectVariants -R /public/home/sxgou_gibh/Genomic/Homo_sapiens/UCSC/hg38.chrom.fasta \
				--discordance /public/home/sxgou_gibh/Genomic/Homo_sapiens/gatk/dbsnp_146.hg38.vcf.gz -select-type SNP --restrict-alleles-to BIALLELIC --variant {input} -O {output.snp}
			gatk SelectVariants -R /public/home/sxgou_gibh/Genomic/Homo_sapiens/UCSC/hg38.chrom.fasta \
				--discordance /public/home/sxgou_gibh/Genomic/Homo_sapiens/gatk/dbsnp_146.hg38.vcf.gz -select-type INDEL --restrict-alleles-to BIALLELIC --variant {input} -O {output.indel}
		'''
rule add_col3:
	input:
		snp = 'Split_Variants/{cell}.snp.vcf.gz',
		indel = 'Split_Variants/{cell}.indel.vcf.gz'
	output:
		snp = 'PASS_Variants/{cell}.snp.vcf',
		indel = 'PASS_Variants/{cell}.indel.vcf'
	shell:
		'''
			zgrep -v '#' {input.snp} | awk '$3=$2'| sed 's/ /'$'\t/g' > {output.snp}
			zgrep -v '#' {input.indel} | awk '$3=$2+length($4)-1'| sed 's/ /'$'\t/g' > {output.indel}
		'''
rule HeatMap:
	input:
                'Split_Variants/{cell}.snp.vcf.gz'
	output:
		A_to_C = 'HeatMap_Data/{cell}_AtoC_snp.vcf',
		A_to_G = 'HeatMap_Data/{cell}_AtoG_snp.vcf',
		A_to_T = 'HeatMap_Data/{cell}_AtoT_snp.vcf',
		C_to_A = 'HeatMap_Data/{cell}_CtoA_snp.vcf',
		C_to_G = 'HeatMap_Data/{cell}_CtoG_snp.vcf',
		C_to_T = 'HeatMap_Data/{cell}_CtoT_snp.vcf',
		G_to_A = 'HeatMap_Data/{cell}_GtoA_snp.vcf',
		G_to_C = 'HeatMap_Data/{cell}_GtoC_snp.vcf',
		G_to_T = 'HeatMap_Data/{cell}_GtoT_snp.vcf',
		T_to_A = 'HeatMap_Data/{cell}_TtoA_snp.vcf',
		T_to_C = 'HeatMap_Data/{cell}_TtoC_snp.vcf',
		T_to_G = 'HeatMap_Data/{cell}_TtoG_snp.vcf'
	shell:
		'''
			zcat {input} | java -jar /public/home/sxgou_gibh/Software/shares/snpEff/SnpSift.jar filter "( REF = 'A' ) & ( ALT = 'C' )" > {output.A_to_C}
			zcat {input} | java -jar /public/home/sxgou_gibh/Software/shares/snpEff/SnpSift.jar filter "( REF = 'A' ) & ( ALT = 'G' )" > {output.A_to_G}
			zcat {input} | java -jar /public/home/sxgou_gibh/Software/shares/snpEff/SnpSift.jar filter "( REF = 'A' ) & ( ALT = 'T' )" > {output.A_to_T}
			zcat {input} | java -jar /public/home/sxgou_gibh/Software/shares/snpEff/SnpSift.jar filter "( REF = 'C' ) & ( ALT = 'A' )" > {output.C_to_A}
			zcat {input} | java -jar /public/home/sxgou_gibh/Software/shares/snpEff/SnpSift.jar filter "( REF = 'C' ) & ( ALT = 'G' )" > {output.C_to_G}
			zcat {input} | java -jar /public/home/sxgou_gibh/Software/shares/snpEff/SnpSift.jar filter "( REF = 'C' ) & ( ALT = 'T' )" > {output.C_to_T}
			zcat {input} | java -jar /public/home/sxgou_gibh/Software/shares/snpEff/SnpSift.jar filter "( REF = 'G' ) & ( ALT = 'A' )" > {output.G_to_A}
			zcat {input} | java -jar /public/home/sxgou_gibh/Software/shares/snpEff/SnpSift.jar filter "( REF = 'G' ) & ( ALT = 'C' )" > {output.G_to_C}
			zcat {input} | java -jar /public/home/sxgou_gibh/Software/shares/snpEff/SnpSift.jar filter "( REF = 'G' ) & ( ALT = 'T' )" > {output.G_to_T}
			zcat {input} | java -jar /public/home/sxgou_gibh/Software/shares/snpEff/SnpSift.jar filter "( REF = 'T' ) & ( ALT = 'A' )" > {output.T_to_A}
			zcat {input} | java -jar /public/home/sxgou_gibh/Software/shares/snpEff/SnpSift.jar filter "( REF = 'T' ) & ( ALT = 'C' )" > {output.T_to_C}
			zcat {input} | java -jar /public/home/sxgou_gibh/Software/shares/snpEff/SnpSift.jar filter "( REF = 'T' ) & ( ALT = 'G' )" > {output.T_to_G}
		'''
rule CtoT_clean:
	input:
		C_to_T = 'HeatMap_Data/{cell}_CtoT_snp.vcf',
		C_to_A = 'HeatMap_Data/{cell}_CtoA_snp.vcf',
		C_to_G = 'HeatMap_Data/{cell}_CtoG_snp.vcf',
		A_to_G = 'HeatMap_Data/{cell}_AtoG_snp.vcf',
		A_to_T = 'HeatMap_Data/{cell}_AtoT_snp.vcf',
		A_to_C = 'HeatMap_Data/{cell}_AtoC_snp.vcf'
	output:
		C_to_T = 'CtoT_annovar/Input/{cell}_CtoT.filtered.snp.vcf',
		C_to_A = 'CtoT_annovar/Input/{cell}_CtoA.filtered.snp.vcf',
		C_to_G = 'CtoT_annovar/Input/{cell}_CtoG.filtered.snp.vcf',
		A_to_G = 'CtoT_annovar/Input/{cell}_AtoG.filtered.snp.vcf',
		A_to_T = 'CtoT_annovar/Input/{cell}_AtoT.filtered.snp.vcf',
		A_to_C = 'CtoT_annovar/Input/{cell}_AtoC.filtered.snp.vcf'
	shell:
		'''
			grep -v '#' {input.C_to_T} | awk '$3=$2'| sed 's/ /'$'\t/g' > {output.C_to_T}
			grep -v '#' {input.C_to_A} | awk '$3=$2'| sed 's/ /'$'\t/g' > {output.C_to_A}
			grep -v '#' {input.C_to_G} | awk '$3=$2'| sed 's/ /'$'\t/g' > {output.C_to_G}
			grep -v '#' {input.A_to_G} | awk '$3=$2'| sed 's/ /'$'\t/g' > {output.A_to_G}
			grep -v '#' {input.A_to_T} | awk '$3=$2'| sed 's/ /'$'\t/g' > {output.A_to_T}
			grep -v '#' {input.A_to_C} | awk '$3=$2'| sed 's/ /'$'\t/g' > {output.A_to_C}
		'''
rule annovar:
	input:
		C_to_T = 'CtoT_annovar/Input/{cell}_CtoT.filtered.snp.vcf',
		C_to_A = 'CtoT_annovar/Input/{cell}_CtoA.filtered.snp.vcf',
		C_to_G = 'CtoT_annovar/Input/{cell}_CtoG.filtered.snp.vcf',
		A_to_G = 'CtoT_annovar/Input/{cell}_AtoG.filtered.snp.vcf',
		A_to_T = 'CtoT_annovar/Input/{cell}_AtoT.filtered.snp.vcf',
		A_to_C = 'CtoT_annovar/Input/{cell}_AtoC.filtered.snp.vcf'
	output:
		C_to_T = 'CtoT_annovar/{cell}_CtoT.snp.hg38_multianno.txt',
		C_to_A = 'CtoT_annovar/{cell}_CtoA.snp.hg38_multianno.txt',
		C_to_G = 'CtoT_annovar/{cell}_CtoG.snp.hg38_multianno.txt',
		A_to_G = 'CtoT_annovar/{cell}_AtoG.snp.hg38_multianno.txt',
		A_to_T = 'CtoT_annovar/{cell}_AtoT.snp.hg38_multianno.txt',
		A_to_C = 'CtoT_annovar/{cell}_AtoC.snp.hg38_multianno.txt',
	shell:
		'''
			table_annovar.pl {input.C_to_T} ~/Software/shares/annovar/humandb/ -buildver hg38 -out CtoT_annovar/{wildcards.cell}_CtoT.snp -remove -protocol refGene,avsnp150 -operation gx,f -nastring .
			table_annovar.pl {input.C_to_A} ~/Software/shares/annovar/humandb/ -buildver hg38 -out CtoT_annovar/{wildcards.cell}_CtoA.snp -remove -protocol refGene,avsnp150 -operation gx,f -nastring .
			table_annovar.pl {input.C_to_G} ~/Software/shares/annovar/humandb/ -buildver hg38 -out CtoT_annovar/{wildcards.cell}_CtoG.snp -remove -protocol refGene,avsnp150 -operation gx,f -nastring .
			table_annovar.pl {input.A_to_G} ~/Software/shares/annovar/humandb/ -buildver hg38 -out CtoT_annovar/{wildcards.cell}_AtoG.snp -remove -protocol refGene,avsnp150 -operation gx,f -nastring .
			table_annovar.pl {input.A_to_T} ~/Software/shares/annovar/humandb/ -buildver hg38 -out CtoT_annovar/{wildcards.cell}_AtoT.snp -remove -protocol refGene,avsnp150 -operation gx,f -nastring .
			table_annovar.pl {input.A_to_C} ~/Software/shares/annovar/humandb/ -buildver hg38 -out CtoT_annovar/{wildcards.cell}_AtoC.snp -remove -protocol refGene,avsnp150 -operation gx,f -nastring .
		'''
rule annovar_all:
	input:
		snp = 'PASS_Variants/{cell}.snp.vcf',
                indel = 'PASS_Variants/{cell}.indel.vcf'
	output:
		snp = "Annovar_all/{cell}.snp.hg38_multianno.txt",
		indel = "Annovar_all/{cell}.indel.hg38_multianno.txt"
	shell:
		'''
			table_annovar.pl {input.snp} ~/Software/shares/annovar/humandb/ -buildver hg38 -out Annovar_all/{wildcards.cell}.snp -remove -protocol refGene,avsnp150 -operation gx,f -nastring .
			table_annovar.pl {input.indel} ~/Software/shares/annovar/humandb/ -buildver hg38 -out Annovar_all/{wildcards.cell}.indel -remove -protocol refGene,avsnp150 -operation gx,f -nastring .
		'''
